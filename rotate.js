import java.io.*;
import java.util.StringTokenizer;
public class Main
{
    /*Method to rotate array to the index provided
    @params array to rotate
    @params rotateIndex
    return void*/
    public static void rotateArray(int[] testArray, int rotateIndex){
       for(int i = rotateIndex; i < testArray.length + rotateIndex; i++) 
            System.out.print(testArray[i % testArray.length] + " "); 
        System.out.println(); 
    }
    
    public static void main(String[] args) throws Exception{
	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		int noOFTestCase = Integer.parseInt(br.readLine());
		for(int index = 0; index < noOFTestCase; index++){
            try{
                StringTokenizer st = new StringTokenizer(br.readLine());
                int sizeOFArray = Integer.parseInt(st.nextToken());
		        int[] testArray = new int[sizeOFArray];
		        
		        int rotateIndex = Integer.parseInt(st.nextToken());
		         
		        StringTokenizer arrInput = new StringTokenizer(br.readLine());
    		    for(int i = 0; i < testArray.length; i++){
    		        testArray[i] = Integer.parseInt(arrInput.nextToken());
    		    }
    		    rotateArray(testArray, rotateIndex);
		    }catch(NumberFormatException ex){ 
		        // handle your exception
                System.out.println("not a number"); 
            }    
		}
	}
}