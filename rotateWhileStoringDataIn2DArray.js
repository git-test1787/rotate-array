import java.util.*;
public class Main
{
    /*Method to rotate array to the index provided
    @params array to rotate
    @params rotateIndex
    return void*/
    public static void rotateArray(long[] testArray, int rotateIndex){
       while(rotateIndex-->0){
            long firstIndexElement = testArray[0];
            for(int index = 1; index < testArray.length; index++){
                testArray[index-1] = testArray[index];
            }
            testArray[testArray.length-1] = firstIndexElement;
        }
    }
    
    /*Method to print the array
    @params 2D Array
    return void*/
    public static void printRotatedResult(long[][] testArray){
        for(int i = 0; i < testArray.length; i++){
		    for(int j = 0; j < testArray[i].length; j++ ){
		        System.out.print(testArray[i][j] + " ");
		    }
		    System.out.println();
		}
    }
    
	public static void main(String[] args) throws Exception{
	    Scanner sc = new Scanner(System.in);
		int noOFTestCase = sc.nextInt();
		if(noOFTestCase < 1 || noOFTestCase > 200)
		    throw new Exception();
		long[][] testArray = new long[noOFTestCase][2];
		sc.nextLine();
		
        for(int index = 0; index < noOFTestCase; index++){
            try{
                String s[] = sc.nextLine().split(" ");
		        testArray[index] = new long[Integer.parseInt(s[0])];
		        
		        int rotateIndex = Integer.parseInt(s[1]);
		        if(rotateIndex < 1 || rotateIndex > Integer.parseInt(s[0]))
		            throw new Exception();
		            
		        s = sc.nextLine().split(" ");
    		    for(int i = 0; i < testArray[index].length; i++){
    		        testArray[index][i] = Long.parseLong(s[i]);
    		    }
    		    rotateArray(testArray[index], rotateIndex);
		    }catch(NumberFormatException ex){ 
		        // handle your exception
                System.out.println("not a number"); 
            }    
		}
		printRotatedResult(testArray);
	}
}